# Beanworks Mac Setup File

Has installation and include files for a typical Beanworks Mac installation.

Files named `*.install.sh` are installation scripts. 

`install.sh` shows you the correct order. It's probably not yet workable as a stand-alone script.

Files named `*.include.sh` are meant to be sourced in your profile.

Recommended use: **fork** this repository. 

**YOU PROBABLY SHOULDN'T ACTUALLY RUN `install.sh` YET.**

Proceed as follows:
```
git clone <your forked repo> 
cd <repo name>

./brew.install.sh

./oh-my-zsh-install.sh

./java.install.sh

./js.install.sh

./dcm.install.sh
```
if you want to build full beanworks services, run

```
./beanworks.install.sh
```
if you only need to build ui, run 
```
./beanworks.install.ui_only.sh
```
Two other installation scripts:

```
./vscode.install.sh
./utilities.install.sh
```

**Use it as a guide for what you need to run.**
### Note 
Running `beanworks.install.ui_only.sh` will also starts a live webpack server to serve js and css on port 4443, so when you are done with it , please `kill $(pgrep webpack)` to kill the 
local webpack server.


